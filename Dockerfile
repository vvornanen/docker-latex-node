FROM ubuntu:19.10
ENV DEBIAN_FRONTEND noninteractive

# TODO: Huge texlive-latex-extra needed only for texlive-latex-extra, drop deprecated subfigure
RUN apt-get update -q && apt-get install -qy \
    texlive texlive-lang-european texlive-latex-extra \
    git curl

RUN curl -sL https://deb.nodesource.com/setup_12.x | bash -
RUN apt-get install -qy nodejs && rm -rf /var/lib/apt/lists/*

WORKDIR /data
VOLUME ["/data"]
